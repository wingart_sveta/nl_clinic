
$(function(){
// IPad/IPhone
	var viewportmeta = document.querySelector && document.querySelector('meta[name="viewport"]'),
	ua = navigator.userAgent,

	gestureStart = function () {viewportmeta.content = "width=device-width, minimum-scale=0.25, maximum-scale=1.6";},

	scaleFix = function () {
		if (viewportmeta && /iPhone|iPad/.test(ua) && !/Opera Mini/.test(ua)) {
			viewportmeta.content = "width=device-width, minimum-scale=1.0, maximum-scale=1.0";
			document.addEventListener("gesturestart", gestureStart, false);
		}
	};
	
	scaleFix();
	// Menu Android
	if(window.orientation!=undefined){
    var regM = /ipod|ipad|iphone/gi,
     result = ua.match(regM)
    if(!result) {
     $('.menu li').each(function(){
      if($(">ul", this)[0]){
       $(">a", this).toggle(
        function(){
         return false;
        },
        function(){
         window.location.href = $(this).attr("href");
        }
       );
      } 
     })
    }
   } 
});
var ua=navigator.userAgent.toLocaleLowerCase(),
 regV = /ipod|ipad|iphone/gi,
 result = ua.match(regV),
 userScale="";
if(!result){
 userScale=",user-scalable=0"
}
document.write('<meta name="viewport" content="width=device-width,initial-scale=1.0'+userScale+'">')

if($(".owl-carousel").length){
   include("js/owl.carousel.js");
 }

if($(".styler").length){
   include("js/jquery.formstyler.js");
 }

if($(".popup_open").length){
      include("js/jquery.arcticmodal.js"); 
}

if($(".calendar").length){
      include("js/jquery.easing.1.3.js"); 
      include("js/jquery-ui.min.js"); 
}
if($(".fancylink").length){
      include("js/jquery.fancybox.js"); 
     
}
if($(".gmaps").length){
      include("js/gmaps.js"); 
     
}
 //----Include-Function----
function include(url){ 
  document.write('<script src="'+ url + '"></script>'); 
}


$(document).ready(function(){

//---- owl-carousel
    if($(".owl-carousel").length){
      $(".owl-carousel").each(function(){
        var $this = $(this);

          if($this.hasClass('carousel_one')){

              if($this.hasClass('main_carousel')){
             
                  $this.owlCarousel({
                      singleItem:true,
                      addClassActive:true,
                      transitionStyle : "fade"

                  });
                
                }
             
              else{
                $this.owlCarousel({
                    singleItem:true,
                    addClassActive:true,
                    pagination : false,
                    afterMove: function(){
                      
                      if($this.hasClass("show_carousel")){

                        ShowActive1.call($this);

                      }
                    
                    }
                  });
              }
            }        
            else{
              $this.owlCarousel({
                items:4,
                itemsDesktop:[1120,3],
                itemsDesktopSmall:[979,2],
                itemsTablet: [768,2],
                itemsMobile:[479,1]
                
              });
            }
      });
    }

  //----menu categories--------
  if($('.main_categories_inner').length){
    $('.main_categories_inner').on('click', function(){
      $(this).toggleClass('active')
        .next('.subcategories_list')
        .slideToggle()
        .parents(".main_categories_list li")
        .siblings(".main_categories_list li")
        .find(".main_categories_inner")
        .removeClass("active")
        .next(".subcategories_list")
        .slideUp();
      
    });

  }
  //--------------googlemaps---------
  if ($(".gmaps").length){

    new GMaps({
      div: '#map_box',
      lat: 59.852121,
      lng: 30.284155
    });

    map = new GMaps({
      div: '#map_box',
      lat: 59.852121,
      lng: 30.284155,
      zoom:14,
      panControl: false,
      zoomControl: true,
      mapTypeControl: false,
      scrollwheel: false
    });

    map.addMarker({
        lat: 59.852078,
        lng: 30.286601,
        icon: 'http://test1.wecoders.net/nl_clinic/images/icon/marker_maps.png'
    });
  }
  //-- formstyler--------------------
      if($(".styler").length){
          $(".styler").styler();  
      }
  //----arcticmodal-------
  if(".popup_open".length){
      $(".popup_open").on('click',function(){
          var modal = $(this).data("modal");
          $(modal).arcticmodal();
      });
    }
//---------calendar------
 if($(".calendar").length){
    $( ".calendar" ).datepicker({
      altField: "#actualDate"
    });
  }
//---fancybox--------------------  
  if($(".fancylink").length){
    $(".fancylink").fancybox({
      openEffect  : 'none',
      closeEffect : 'none'
    });
  }
//-------------Show_menu responsive-------------
$("#menu_overlay").on("click", function(){
      if($("body").hasClass("show_menu")){
        $("body").removeClass("show_menu");
        $(this).fadeOut();
      }
    })
/*------------Close_menu------------*/
 $(".close").on("click", function(){
          $(this).parent().removeClass("show_menu");
          $("body").removeClass("show_menu");
      });
//---------show menu responsive-----------------------

  $(".toggle_menu_btn").on("click", function(){
    $("body").toggleClass("show_menu");
  })
 
});


$(window).load(function(){

  ShowActive();

});

if($(".show_carousel").length){

    function ShowActive(){

      $(".show_carousel").each(function(){

        var ShowItemAll=$(this).find('.owl-item').length,
            ActiveItem= $(this).find('.owl-item.active').index() +1,
            ShowActiveItem=ActiveItem + '/' + ShowItemAll;
      
        $(this).find('.owl-controls').append($('<div class="qt"></div>').text(ShowActiveItem));

      });


    }

    function ShowActive1(){

        var ShowItemAll=this.find('.owl-item').length,
            ActiveItem= this.find('.owl-item.active').index() +1,
            ShowActiveItem=ActiveItem + '/' + ShowItemAll;
      
        this.find('.qt').text(ShowActiveItem);


    }
}

 




